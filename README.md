# Комментарии для сниппетов

## :clapper: Навигация
1. [Основная программа, русский язык](https://gitlab.com/h3yAlias/common-comments/-/blob/new-syllabus/src/html-css/ru/common-comments.json)
2. [Основная программа, английский язык](https://gitlab.com/h3yAlias/common-comments/-/blob/new-syllabus/src/html-css/en/common-comments.json)
3. [Старая программа, русский язык (N.b пока в этом нет смысла) ](https://gitlab.com/h3yAlias/common-comments/-/blob/old-web/src/html-css/ru/common-comments.json)
4. [Старая программа, английский язык (N.b пока в этом нет смысла)](https://gitlab.com/h3yAlias/common-comments/-/blob/old-web/src/html-css/en/common-comments.json)


## :thinking: Описание
Набор сниппетов для Tampermonkey

### Установка
1. [Скачать расширение](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=ru) ([Opera](https://addons.opera.com/ru/extensions/details/tampermonkey-beta/))
2. В настройках расширения добавить новый пользовательский скрипт.
[Ссылка на скрипт](https://gitlab.com/h3yAlias/common-comments/-/blob/master/lib/tampermonkey.js).
В поле `@match` или `@include` пользовательского скрипта вписать `https://praktikum-admin.yandex-team.ru/office/revisor-review/*` 
3. [Скачать расширение](https://chrome.google.com/webstore/detail/disable-content-security/ieelmcmcagommplceebfedjlakkhpden). 
Включать на вкладке с ревизорро

## :clap: Использование
1. Попап появляется на странице [Ревизорро](https://praktikum-admin.yandex-team.ru/office/revisor-review/)
2. Выбираете необходимый пресет сниппетов: язык и программу. 
3. Сабмитите форму
4. В поле поиска вводите ключ или его часть. Код ключа - это ключ объекта в JSON
    <pre><code>
      "1-c-need_paragraph": {
       
      },
    </code></pre>
5. Выбираете подходящий комментарий, нажимаете на него. Он копируется в буфер обмена.
6. Вставляете скопированный текст в поле комментария в ревизорро.
7. :thinking: [Demo](https://yadi.sk/i/Jd5LcMZF_QGHSQ) :thinking:

*Актуальные ветки: `new-syllabus` для новой программы. `old-syllabus` для легаси.*

## :no_entry: Внесение изменений

### [Issues](https://gitlab.com/h3yAlias/common-comments/-/issues)

### Если есть что-то, что хотите запушить в проект:
* Pull-request are welcome. Изменяйте только необходимую ветку
* Пишите короткие, содержательные коммиты. Без атомарщины. Пользуйтесь [этим](https://www.conventionalcommits.org/ru/v1.0.0-beta.4/)
* Один коммит - один JSON-объект.

## Строение ключа (Пример `1-c-need_paragraph`)
1. Через кебаб-кейс описываются дополнительные параметры:
    1. 1 - номер спринта. Опционально: если ошибка характерна для нескольких спринтов, то спринт можно не указывать
    2. c - тип ошибки. `C` - critical. `I` - improvement. `G` - good job
2. Через снэйк_кейс описываются основные параметры, название ключа. Хорошие неймспейсы:
    1. `need` - необходимость добавить тот или иной элемент, либо часть лэйаута (Например, ховер)
    2. `incorrect` - непосредственна ошибка. Неправильное название по бэм, f.e
    3. `extra` - указатель на лишний элемент, либо часть лэйаута или бэм-сущности.
