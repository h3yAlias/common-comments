class Model {
  createElement(markup) {
    const markupElement = document.createElement(`div`);
    markupElement.innerHTML = markup;
    return markupElement.firstChild;
  }
}

export { Model };
