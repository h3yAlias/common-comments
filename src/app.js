import { SectionList } from "./layout/section-list";
import { CacheProvider } from "./util/cacheProvider";
import {
  apiConfig,
  fieldsetConfig,
  localStorageKeysConfig,
  presetConfig,
  searchConfig,
  headerConfig,
  widgetConfig,
  suggestionListConfig,
} from "./util/config";
import { Api } from "./layout/api";
import { Search } from "./component/search";
import { SuggestionItem } from "./component/suggestion-item";
import { Preset } from "./component/preset";
import { Header } from "./component/header";
import { Widget } from "./component/widget";
import has from "./util/has";

export class App {
  constructor (element) {
    this.wrap = new Widget({ widgetConfig });
    this.viewController = new SectionList({ element, wrap: this.wrap });
    this.cache = new CacheProvider(localStorageKeysConfig);
    this.header = new Header({ status: 'Инициализация', headerConfig });
    this.viewController.renderElement(this.header.getView());
    this.viewController.applyStyles(this.header.getStyles());
  }


  changeStatus (newStatus, timeout = 0) {
    console.log('status changed', newStatus);
    this.header.changeStatus(newStatus, timeout);
  }

  run () {
    this.cache.getPreset()
      .then((userChoice) => this.afterUserChoice(userChoice))
      .catch((e) => {
        console.error('Load from cache failed', e);
        this.showForm().then(userChoice => {
          this.cache.setPreset(userChoice)
            .catch((e) => console.log(e));
          this.afterUserChoice(userChoice);
        })
          .catch(e => console.log('Cannot render form', e))
      });
  }

  afterUserChoice (userChoice) {
    this.changeStatus('Настройки пресета найдены');
    const apiUrl = `${apiConfig.url}src%2F${userChoice.course}%2F${userChoice.lang}%2Fcommon-comments.json?ref=${userChoice.syllabus}`;
    console.info('API Access URL:', apiUrl);
    this.api = new Api({
      url: apiUrl,
      token: apiConfig.token
    });
    this.changeStatus('Загрузка пресета');
    this.api.getAppInfo()
      .then((result) => {
        this.search = new Search({
          onKeyPress: (value) => {
            // TODO fix: change json structure in snippets
            const filteredList = result.find(array => typeof array === "object").filter((element) => {
              return element.find(property => typeof property === "string").toLowerCase().includes(value) ||
                element.find(property => typeof property === "object").description.toLowerCase().includes(value);
            });

            this.viewController.clear(document.body.querySelector(`.${suggestionListConfig.wrap}`));

            if (value.length)
              filteredList.forEach(element => document.body.querySelector(`.${suggestionListConfig.wrap}`)
                .appendChild(new SuggestionItem({
                  data: element,
                  onClick: () => {
                    this.changeStatus('Скопировано!', 1000)

                  },
                  suggestionListConfig
                }).getView()))

          },
          suggestionListConfig,
          searchConfig
        });
        this.viewController.renderElement(this.search.getView());
        this.viewController.applyStyles(this.search.getStyles());
        this.changeStatus(`Пресет ${userChoice.syllabus} (${userChoice.lang})`);
      })
      .catch(e => {
        this.changeStatus('Cannot render elements');
        console.error('Cannot render elements', e);
      })
  }


  showForm () {
    return new Promise((resolve => {
      this.preset = new Preset({
        onSubmit: (formData) => {
          if (!has(formData, 'lang') || !has(formData, 'syllabus')) {
            console.error('Form somehow submitted without one of required fields: lang or syllabus');
          }
          this.preset.remove();
          return resolve(formData);
        },
        presetConfig,
        fieldsetConfig
      });
      this.changeStatus('Выберите пресет');
      this.viewController.renderElement(this.preset.getView());
      this.viewController.applyStyles(this.preset.getStyles())
    }))
  }
}
