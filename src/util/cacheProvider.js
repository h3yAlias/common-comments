/**
 * Work with cache in Localstorage
 */
export class CacheProvider {
  /**
   *
   * @param {{preset:string}} config
   */
  constructor (config) {
    this.config = config
  }

  _isObject (value) {
    return typeof value === 'object' && value !== null
  }

  _get (key) {
    return new Promise(((resolve, reject) => {
      if (!window.localStorage) {
        return reject('LocalStorage in not found')
      }
      const userChoice = window.localStorage.getItem(key);
      return resolve(JSON.parse(userChoice));
    }))
  }

  /**
   *
   * @param {string} key - Localstorage key
   * @param {Object|string} value - Value to set
   * @returns {Promise<void>}
   * @private
   */
  _set (key, value) {
    return new Promise(((resolve, reject) => {
      if (!window.localStorage) {
        return reject('LocalStorage in not found')
      }
      let toSet = '';
      if (this._isObject(value)) {
        toSet = JSON.stringify(value)
      }
      window.localStorage.setItem(this.config.preset, toSet);
      resolve();
    }))
  }

  setPreset (value) {
    return this._set(this.config.preset, value)
  }

  getPreset () {
    return this._get(this.config.preset)
  }
}
