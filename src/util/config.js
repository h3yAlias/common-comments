export const presetConfig = {
  wrap: 'preset',
  innerWrap: 'preset__box',
  group: 'preset__fieldset',
  button: 'preset__button',
  radio: 'radio',
  hiddenInput: 'radio__box',
  inputBox: 'radio__span',
};
export const headerConfig = {
  wrap: 'pcr-cc-header',
  status: 'pcr-cc-status',
  settingsBtn: 'pcr-cc-settings'
};
export const widgetConfig = {
  wrap: 'pcr-cc-widget',
};
/**
 * @typedef {Object} FieldSetConfig
 * @property {string[]} values
 * @property {string} title
 */
/** @type {FieldSetConfig[]} */
export const fieldsetConfig = [
  {
    values: ['ru', 'en'],
    title: 'Выберите язык',
    key: 'lang'
  },
  {
    values: ['new-syllabus', 'old-web'],
    title: 'Выберите программу',
    key: 'syllabus'
  },
  {
    values: ['html-css', 'js'],
    title: 'Выберите курс',
    key: 'course'
  }
];
export const localStorageKeysConfig = Object.freeze({
  preset: 'pcr-cc-preset'
});
export const searchConfig = {
  wrap: 'search-form',
  input: 'search-form__item'
};

export const suggestionListConfig = {
  wrap: 'suggestion-list',
  item: 'suggestion-item',
  text: 'suggestion-item__text'
};

export const apiConfig = {
  url: 'https://gitlab.com/api/v4/projects/17796416/repository/files/',
  token: '7pccdufMvDNZdZpVVcau'
};
