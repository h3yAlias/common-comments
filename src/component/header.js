import { Model } from "../model/model";
import { headerStyles } from "./styles/headerStyles";


class Header extends Model {
  constructor ({ status, ...config }) {
    super();
    this._config = config;
    this._status = status;
    this._statusNode = undefined;
  }

  _getTemplate () {
    return `
      <div class="${this._config.headerConfig.wrap}">
          <p class="${this._config.headerConfig.status}">${this._status}</p>

      </div>`.trim()
  }

  getStyles () {
    return headerStyles(this._config);
  }

  changeStatus (status, timeout = 0) {
    let oldStatus = this._status;
    this._status = status;
    this._statusNode.innerText = this._status;
    if (timeout !== 0 && oldStatus !== this._status) {
      setTimeout(() => this.changeStatus(oldStatus), timeout)
    }
  }

  getView () {
    this._element = super.createElement(this._getTemplate());
    this._statusNode = this._element.querySelector('.' + this._config.headerConfig.status);
    return this._element;
  }

  remove () {
    this._element.remove();
  }
}

export { Header };
