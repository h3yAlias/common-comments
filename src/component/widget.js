import { Model } from "../model/model";
import { widgetStyles } from "./styles/widgetStyles";

class Widget extends Model {

  constructor ({ ...config }) {
    super();
    this._config = config;
  }

  _getTemplate () {
    return `<div class="${this._config.widgetConfig.wrap}"><style id="pcr-cc-styles"></style> </div>`;
  }

  getStyles () {
    return widgetStyles(this._config);
  }

  getView () {
    this._element = super.createElement(this._getTemplate());
    return this._element;
  }

  remove () {
    this._element.remove();
  }
}

export { Widget }
