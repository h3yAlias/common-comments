import { Model } from '../model/model.js'

class SuggestionItem extends Model {
  constructor({data, onClick, ...config}) {
    super();
    this._config = config;
    this._filteredObject = data.find(element => element.description && element.type);
    this.clickCallback = onClick || (() => {});
    this._handleItemClick = this._handleItemClick.bind(this);

    this._element = null;
  }

  _getTemplate() {
    return `
      <div class="${this._config.suggestionListConfig.item}">
        <p class="${this._config.suggestionListConfig.text}">${this._filteredObject.description}</p>
        <p class="${this._config.suggestionListConfig.text}">${this._filteredObject.type}</p>
     </div>
    `.trim();
  }

  _handleItemClick() {
    navigator.clipboard.writeText(this._filteredObject.description)
      .then(() => {
        console.log('copied');
        this.clickCallback?.();
      })
      .catch(e => console.log('cant copy text'))
  }

  _setEventListeners() {
    this._element.addEventListener('click', this._handleItemClick)
  }

  getView() {
    this._element = super.createElement(this._getTemplate());
    this._setEventListeners();
    return this._element;
  }
}

export { SuggestionItem };
