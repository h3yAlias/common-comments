export const searchStyles = (config) => `
    .${config.searchConfig.wrap} {
      padding:20px
    }

    .${config.searchConfig.input} {
      width: 100%;
      height: 100%;
      font-size: 20px;
      font-family: 'Helvetica Neue', Arial, sans-serif;
    }

    .${config.suggestionListConfig.wrap}::-webkit-scrollbar {
      width: 5px;
    }

    .${config.suggestionListConfig.wrap}::-webkit-scrollbar-thumb {
      background: #f8971d;
      border-radius: 20px;
    }

    .${config.suggestionListConfig.wrap}::-webkit-scrollbar-track {
      background: #ddd;
      border-radius: 20px;
    }

    .${config.suggestionListConfig.wrap} {
      max-height: 200px;
      overflow: auto;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      margin: 20px 0;
    }
    .${config.suggestionListConfig.item} {
      background: white;
      display: flex;
      flex-direction: column;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      -webkit-transition: 0.3s;
      -moz-transition: 0.3s;
      -ms-transition: 0.3s;
      -o-transition: 0.3s;
      transition: 0.3s;
      cursor: pointer;
      padding: 10px;
    }
    .${config.suggestionListConfig.item}:hover {
      background: #f5f5f5;
    }
    .${config.suggestionListConfig.text} {
      font-size: 14px;
      margin: 5px 0;
    }`;
