export const presetStyles = (config) => `
    .${config.presetConfig.wrap} {
      padding: 20px;
      background: white;
      }
    .${config.presetConfig.radio} {
      display: flex;
      align-items: center;
      cursor: pointer;
      font-size: 22px;
    }

    .${config.presetConfig.hiddenInput}:checked + .${config.presetConfig.inputBox}::before {
      width: 50%;
      height: 50%;
    }

    .${config.presetConfig.hiddenInput} {
      line-height: 0;
      font-size: 0;
      position: absolute;
      opacity: 0;
    }

    .${config.presetConfig.inputBox} {
      width: 20px;
      height: 20px;
      border-radius: 25%;
      background-color: transparent;
      border: 1px solid black;
      display: block;
      flex-shrink: 0;
      position: relative;
      margin: 5px 10px 0 0;
    }

    .${config.presetConfig.inputBox}::before {
      content: '';
      width: 0;
      height: 0;
      background-color: black;
      display: block;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translateX(-50%) translateY(-50%);
      border-radius: 25%;
      transition: 0.3s;
    }
    .${config.presetConfig.group} {
      margin: 10px 0;
    }

    .${config.presetConfig.button} {
      height: 50px;
      color: #ffffff;
      border: none;
      background-color: #1a1b22;
      padding: 17px 14px 16px;
      font-size: 18px;
      box-sizing: border-box;
      box-shadow: none;
      line-height: 17px;
      font-weight: 400;
      border-radius: 2px;
      text-transform: none;
    }`;
