export const widgetStyles = (config) => `
    .${config.widgetConfig.wrap} {
      background: black;
      position: fixed;
      top: -1px;
      left: calc(100vw - 420px);
      right: -1px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      z-index: 100;
      border:1px solid white;

      }
`;
