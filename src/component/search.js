import { Model } from '../model/model.js'
import { searchStyles } from "./styles/searchStyles";

class Search extends Model {
  constructor ({ onKeyPress, ...config }) {
    super();
    this._config = config;
    this._onKeyPress = onKeyPress || (() => {});
    this._onKeyPress = this._onKeyPress.bind(this);

    this._element = null;
  }

  _getTemplate () {
    return `
      <form class="${this._config.searchConfig.wrap}">
        <input class="${this._config.searchConfig.input}" type="search" placeholder="Начните писать...">
        <div class="${this._config.suggestionListConfig.wrap}">
        </div>
      </form>`.trim()
  }

  _setEventListeners () {
    this._element.querySelector(`.${this._config.searchConfig.input}`)
      .addEventListener('input', () => this._onKeyPress(this._element
        .querySelector(`.${this._config.searchConfig.input}`).value.toLowerCase()));
  }

  getStyles () {
    return searchStyles(this._config)
  }

  getView () {
    this._element = super.createElement(this._getTemplate());
    this._setEventListeners();
    return this._element;
  }
}

export { Search };
