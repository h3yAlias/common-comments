import { Model } from '../model/model.js'
import { presetStyles } from "./styles/presetStyles";

class Preset extends Model {
  constructor ({ onSubmit, ...config }) {
    super();
    this._config = config;
    this._callback = onSubmit;
    this._element = null;
  }

  _onSubmit (evt) {
    if (!this._element.checkValidity()) return;
    evt.preventDefault();
    const formData = Object.fromEntries(new FormData(this._element));
    this._callback?.(formData);
  }

  _getTemplate () {
    return `
      <form action="/" class="${this._config.presetConfig.wrap}">
          <div class="${this._config.presetConfig.innerWrap}">
            ${this._config.fieldsetConfig.map(item => `<fieldset class="${this._config.presetConfig.group}">${this._renderFieldset(item)}</fieldset>`.trim())}
          </div>
          <button class="${this._config.presetConfig.button}" type="submit">Get JSON</button>
      </form>`.trim()
  }

  /**
   *
   * @param {FieldSetConfig} item
   * @returns {string}
   * @private
   */
  _renderFieldset (item) {
    const { values, title, key } = item;
    let markup = `<legend>${title}</legend>`;
    values.forEach((propertyValue) =>
      markup += `
            <label class="${this._config.presetConfig.radio}">
              <input required class="${this._config.presetConfig.hiddenInput}"
                type="radio" name="${key}" value="${propertyValue}">
              <span class="${this._config.presetConfig.inputBox}"></span>
              ${propertyValue}
            </label>`
    );
    return markup;
  }

  getView () {
    this._element = super.createElement(this._getTemplate().replace(/,/g, ''));
    this._setEventListeners();
    return this._element;
  }

  getStyles () {
    return presetStyles(this._config);
  }

  _setEventListeners () {
    this._element.addEventListener('submit', evt => this._onSubmit(evt))
  }

  remove () {
    this._element.remove();
  }

}

export { Preset };
