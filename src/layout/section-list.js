class SectionList {
  constructor ({ element, wrap }) {
    this._elementNode = element;
    this.renderElement(wrap.getView());
    this.applyStyles(wrap.getStyles());
    this._elementNode = this._elementNode.lastElementChild;
  }

  renderElement (markup) {
    this._elementNode.appendChild(markup);
  }

  applyStyles (stylesString) {
    let currentStyles = this._elementNode.querySelector('#pcr-cc-styles');
    this._elementNode.querySelector('#pcr-cc-styles').innerHTML = currentStyles.innerHTML + stylesString;
  }

  clear (element) {
    element.innerHTML = '';
  }
}

export { SectionList };
