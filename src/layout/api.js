import { Base64 } from 'js-base64';

class Api {
  constructor({url, token}) {
    this._url = url;
    this._token = token;
  }

  getAppInfo() {
    return Promise.all([this.getComments()]);
  }

  getComments() {
    return fetch(this._url, {
      method: 'GET',
      headers: {
        "private-token": this._token,
        'Content-Type': 'application/json; charset=utf-8',
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
        res.ok ? res.json() : Promise.reject();
      })
      .then((res) => {
        const result = JSON.parse(Base64.decode(res.content));
        return Array.from(Object.entries(result));
      })
      .catch(e => console.log('cant get json'))

  }
}

export { Api };
