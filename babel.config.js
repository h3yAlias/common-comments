const presets = [
    [
        "@babel/env",
        {
            targets: {
                firefox: "60",
                chrome: "64",
                safari: "11.1",
            },
            useBuiltIns: "usage",
            corejs: "3.0.0",
        },
    ],
];

module.exports = { presets };
